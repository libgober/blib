+++
author = "Brian Libgober"
+++

<!--
This file is left intentionally empty by default to be backwards compatible with the initial theme setup.

Although the theme has advanced a little bit and it now allows to specify the content on the main page (even if the list of posts/articles is not intended).
This can be:
- with the list of posts/articles (default: `mainSections = ["post"]) or
- without the list of posts/articles (by setting `mainSections = [""]`)

Markdown supported, ie:

```
# Welcome

- Hugo :rocket:
- Hugo theme :rocket:

Don't forget to check the README.md file!
```

Remember that you can also specify a section header for the posts below by configuring the `mainSectionsTitle` parameter in the front matter of this file.
-->

 
Brian Libgober is a political scientist and legal scholar. His research focuses on the political economy of American institutions, with a special emphasis on the making of regulations by executive agencies. Thematically, he is interested in the relationship between economic inequality, interest group power, and the design of legal institutions. Methodologically, his work combines a variety of approaches, including formal models, quantitative empirics, and case studies.

Since 2021, Brian has been an Assistant Professor at the University of California, San Diego in the School of Global Policy and Strategy. He is also an affiliate of the Penn Program on Regulation. Previously, he was a postdoctoral fellow in Political Science at Yale. 

His research has been published in the *Journal of Politics*, the *Quantitative Journal of Political Science*, and other peer-reviewed journals. He has also published a number of articles in other journal formats, including a law review article called *Getting a Lawyer While Black* that uses experimental methods to identify and explain discrimination in the market for legal services.

<br>

---

<br>

## Education
<ul class="fa-ul">
  <li style="font-size:18px;"><span class="fa-li"><i class="fas fa-graduation-cap"></i></span>
    Ph.D. in Political Science, 2018<br>
    <em style="font-size:16px">Harvard University</em></li>
  <li style="font-size:18px;"><span class="fa-li">
    <i class="fas fa-graduation-cap"></i></span>
    M.A. in Statistics, 2017<br>
    <em style="font-size:16px;">Harvard University</em>
  </li>
  <li style="font-size:18px;"><span class="fa-li"><i class="fas fa-graduation-cap"></i></span>
    J.D., 2015<br>
    <em style="font-size:16px">University of Michigan Law School</em>
  </li>
  <li style="font-size:18px;"><span class="fa-li"><i class="fas fa-graduation-cap"></i></span>
    B.A. in Mathematics and Philsophy, 2010<br>
    <em style="font-size:16px">University of Chicago</em>
  </li>
</ul>

<br>

---

<br>

# Research

### Recent Working Papers

- [Making Regulators Reasonable: Do Procedural Rationality Requirements Fix Cognitive Bias?](/chen_libgober.pdf)(with Benjamin M. Chen).
- [Agency Failure and Individual Accountability](https://administrativestate.gmu.edu/wp-content/uploads/sites/29/2020/02/Libgober-Agency-Failure-and-Individual-Accountability.pdf). Center for the Study of the Administratiave State, Working Paper No. 20-03.
- [The Political Ideologies of Organized Interests & Amicus Curiae Briefs: Large-Scale, Social Network Imputation of Ideal Points](/amicus_scaling) (with Sahar Abi-Hassan, Janet Box-Steffensmeier, Dino Christenson, and Aaron Kaufman)
- [Personnel, Politics, and the Trump Presidency](/Personnel-Politics-Under-Trump_submission_PLOS_ONE.pdf) (with Mark Richardson). *R&R* at _PLOS: ONE_.
- [Linking Datasets on Organizations Using Half A Billion Open Collaborated Records](/man6.pdf) (with Connor Jerzak).

### Peer-Reviewed Publications

- [Strategic Proposals, Endogenous Comments, and Bias In Rulemaking](https://www.journals.uchicago.edu/doi/10.1086/706891). 2020. *Journal of Politics*.
- [Meetings, Comments, and the Distributive Politics of Rulemaking](http://dx.doi.org/10.1561/100.00018135). 2020. *Quarterly Journal of Political Science*.
- [Data and methods for analyzing special interest influence in rulemaking](https://doi.org/10.1057/s41309-020-00094-w) (with Daniel Carpenter, Devin Judge-Lord, and Steven Rashin). 2020. *Interest Groups & Advocacy*.

### Other Publications

- [Getting a Lawyer While Black: A Field Experiment](https://law.lclark.edu/live/files/29599-lcb241article2libgobercorrection.pdf). 2020. *Lewis & Clark Law Review*.
- [Can the EU be a Constitutional System Without Universal Access to Judical Review](https://repository.law.umich.edu/mjil/vol36/iss2/4/). 2015. *Michigan Journal of International Law*. 

<br>

---

<br>

# Teaching

## UC San Diego

- Policy Analysis and Public Welfare: An Introduction to the Policymaking Process. Fall 2021.
- Law and Adminisistration. Spring 2021. [Syllabus](/).
- Bureaucratic Politics. Spring 2021. [Syllabus](/)
- Interest Groups and the Policymaking Process. Winter 2020. [Syllabus](/)

## Yale University

- American Mass Media: Law, Politics, and Policy. Fall 2019. [Syllabus](/)
- Money in Politics. Spring 2019. [Syllabus](/)

## Harvard University

- Formal Models of Domestic Politics. Spring 2017. (Teaching Assistant; Primary Instructor: Horacio Larreguy).
- Bureaucratic Politics (Teaching Assistant; Primary Instructor: Daniel Carpenter)

## MIT

- Constitutional Law & Judicial Politics. Spring 2015. (Course Assistant; Primary Instructor: Christopher Warshaw).

## University of Michigan

- Mass Media and Election Politics. Fall 2013. (Graduate Student Instructor; Primary Instructor: Nicholas Valentino).

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

